package com.bootcamp.ecommercespringbootapplication.Repository;

import com.bootcamp.ecommercespringbootapplication.Entity.CartItem;
import com.bootcamp.ecommercespringbootapplication.Entity.Product;
import com.bootcamp.ecommercespringbootapplication.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<CartItem, Long> {
    List<CartItem> findByUser(User user);
    CartItem findByUserAndProduct(User user, Product product);
}

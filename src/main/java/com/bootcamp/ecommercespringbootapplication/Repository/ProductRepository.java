package com.bootcamp.ecommercespringbootapplication.Repository;

import com.bootcamp.ecommercespringbootapplication.Entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}

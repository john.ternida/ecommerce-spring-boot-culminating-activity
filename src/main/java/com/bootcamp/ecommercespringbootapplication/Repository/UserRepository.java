package com.bootcamp.ecommercespringbootapplication.Repository;

import com.bootcamp.ecommercespringbootapplication.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
